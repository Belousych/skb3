import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import Promise from 'bluebird';
import Pet from './models/Pet';
import User from './models/User';
import saveDataInDb from './saveDataInDb';

mongoose.Promise = Promise;
mongoose.connect('mongodb://publicdb.mgbeta.ru/belousych_skb3');

const app = express();
app.use(cors());
app.use(bodyParser.json());

app.get('/users', async (req, res) => {
  const users = await User.find();
  return res.json(users);
});

app.get('/pets', async (req, res) => {
  const pets = await Pet.find().populate('owner');
  return res.json(pets);
});

app.post('/data', async (req, res) => {
  const data = req.body;
  // saveDataInDb(data);
  console.log(data);
  // return res.json(data);
  return res.json(await saveDataInDb(data));
});
// const data = {
//   user: {
//     name: 'belousych',
//   },
//   pets: [
//     {
//       name: 'Murz',
//       type: 'cat',
//     },
//     {
//       name: 'Pluto',
//       type: 'dog',
//     },
//   ],
// };
// saveDataInDb(data);

// const app = express();
// app.use(cors());


// const Pet = mongoose.model('Pet', {
//   type: String,
//   name: String,
// });
//
// const kitty = new Pet({
//   type: 'cat',
//   name: 'Murka',
// });
//
// kitty.save()
// .then(() => {
//   console.log('success');
// })
// .catch(() => {
//   console.log('error', err);
// })

app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});

app.listen(3000, () => {
  // console.log('Your app listening on port 3000!');
});
